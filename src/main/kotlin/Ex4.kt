import java.io.File
import java.io.IOException
import java.lang.NumberFormatException
import java.lang.UnsupportedOperationException
import java.nio.file.Path
import java.util.*
import kotlin.NoSuchElementException

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val marcascoches = mutableListOf("BMW", "MERCEDES", "PORSCHE", "AUDI", "SEAT")
    println(marcascoches)
    println("Quina marca vols esborrar?")
    val usermodel = scanner.next().toUpperCase()
    println(editMutableList(usermodel, marcascoches))
}

fun editMutableList(model: String, marcacoches: MutableList<String>): MutableList<String> {
    try {
        marcacoches.remove(model)
    }catch (e:NoSuchElementException){
        println("No s'ha trobat l'element indicat")
    }catch (e:UnsupportedOperationException){
        println("No es posible fer aquesta operació")
    }
    return marcacoches
}