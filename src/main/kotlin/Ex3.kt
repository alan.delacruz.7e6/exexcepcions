import java.io.File
import java.io.IOException
import java.lang.NumberFormatException
import java.nio.file.Path
import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix el document que vols llegir")
    val userfile = scanner.next()
    val file = File("src/main/kotlin/$userfile")
    println(readFileExists(file))
}

fun readFileExists(arxiu: File): List<String> {
    val file = File("$arxiu")
    try {
        return file.readLines()
    }catch (e:IOException){
        println("S'ha produït un error d'entrada/sortida")
    }
    return listOf()
}